Assessment Question are as follows:

1. Write a program for this pattern ?

*
*  *
*  *  *
*  *  *  *
*  *  *  *  *
*  *  *  *  *  *
*  *  *  *  *
*  *  *  *
*  *  *
*  *
*

2. How to find whether a number armstrong or not ?
For example, 371 is an Armstrong number since 3*3*3 + 7*7*7 + 1*1*1 = 371

3. Initially, there is a Robot at position (0, 0). Given a sequence of its moves, judge if this robot makes a circle, which means it moves back to the original place.

The move sequence is represented by a string. And each move is represented by a
character.
The valid robot moves are R (Right), L (Left), U (Up) and D (down).

4. Write a program to print output true or false representing whether the robot makes a circle.

Example 1:
Input: "UD"
Output: true

Example 2:
Input: "LL"
Output: false

5. Write a program for opening and closing time of a shop.

Example 1:
Shop opening time - 10:00:00
Shop closing time - 18:00:00

Current time - 16:00:00

Expected Output - Shop is currently open and closes in 2 hrs.

Example 2:
Shop opening time - 08:00:00
Shop closing time - 15:00:00

Current time - 16:00:00

Expected Output - Shop is currently closed and opens tomorrow at 8:00 AM.

6. Make a library management portal. Requirements are as follows:

Student Module -

i.   Student will be able to signup on their library management portal (In 'inactive' status) [username, email, ].
ii.  Student Login (Only students that are approved by Librarian. Display proper message for inactive user.)
iii. Student will be able to issue a book from BookList [Books that are not issued by other students]
iv.  Student will be able to see listing of his/her issued books.
v.   Student will be able to return issued books.

Librarian Module (Admin) -

i.   Librarian will be able to see listing of newly registered students.
ii.  Librarian will be able to see listing of students.
iii. Librarian will be able to approve/reject any registered students. [status > active or status > rejected]
iv.  Librarian will be able to add, edit and delete books in the system.
v.   Librarian will be able to see listing of un-issued books.
vi.  Librarian will be able to see listing of issued books and student's name.

DB Tables

1. students (Columns- id, username, name, email, password, status['active','inactive','rejected'], created_at, updated_at)
2. books (Columns- id, name, author, price, created_at, updated_at)
3. books_issued (Columns- id, book_id, student_id, created_at, updated_at)



