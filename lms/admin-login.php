<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="./css/admin-login.css">
</head>
<body>
	<button><a href="add-book.php">Add Books</a></button>
	<button><a href="logout.php">Logout</a></button>

	<?php 
		include('config.php');
		$results = mysqli_query($conn, "SELECT * FROM students where status='inactive'"); ?>
		<h2>New Registered Users</h2>
			<table border="1px">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Username</th>
						<th>Status</th>
						<th>Created_at</th>
						<th>Updated_at</th>
						<th>Action</th>
					</tr>
				</thead>
				
				<?php while ($row1 = mysqli_fetch_array($results)) { ?>
					<tr>
						<td><?php echo $row1['name']; ?></td>
						<td><?php echo $row1['email']; ?></td>
						<td><?php echo $row1['username']; ?></td>
						<td><?php echo $row1['status']; ?></td>
						<td><?php echo $row1['created_at']; ?></td>
						<td><?php echo $row1['updated_at']; ?></td>
						<td>
							<a href="admin-login.php?app=<?php echo $row1['id']; ?>">Approve</a>
							<a href="admin-login.php?reject=<?php echo $row1['id']; ?>">Reject</a>
						</td>
					</tr>
				<?php } ?>
			</table>

		<?php

		if(isset($_GET['app'])){
			$id = $_GET['app'];
			mysqli_query($conn, "UPDATE students SET status = 'active' where id=$id");
		}

		if(isset($_GET['reject'])){
			$id = $_GET['reject'];
			mysqli_query($conn, "DELETE from students where id='$id'");
		}

		?>

		<?php
			$results2 = mysqli_query($conn, "SELECT * FROM students where status='active'"); ?>
		    <h2>All Users</h2>
			<table border="1px">
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Username</th>
						<th>Status</th>
						<th>Created_at</th>
						<th>Updated_at</th>
						<th>Action</th>
					</tr>
				</thead>
				
				<?php while ($row2 = mysqli_fetch_array($results2)) { ?>
					<tr>
						<td><?php echo $row2['name']; ?></td>
						<td><?php echo $row2['email']; ?></td>
						<td><?php echo $row2['username']; ?></td>
						<td><?php echo $row2['status']; ?></td>
						<td><?php echo $row2['created_at']; ?></td>
						<td><?php echo $row2['updated_at']; ?></td>
						<td>
							
							<a href="admin-login.php?delete=<?php echo $row2['id']; ?>">Delete</a>
						</td>
					</tr>
				<?php } ?>
			</table>

			<?php 
				if(isset($_GET['delete'])){
					$id = $_GET['delete'];
					mysqli_query($conn, "DELETE FROM students where id=$id");
				}
			?>



        <?php
			$book = mysqli_query($conn, "SELECT * FROM books"); ?>
		    <h2>Books</h2>
			<table border="1px">
				<thead>
					<tr>
						<th>Name</th>
						<th>Author</th>
						<th>Price</th>
						<th>Created_at</th>
						<th>Updated_at</th>
						<th>Action</th>
					</tr>
				</thead>
				
				<?php while ($r = mysqli_fetch_array($book)) { ?>
					<tr>
						<td><?php echo $r['name']; ?></td>
						<td><?php echo $r['author']; ?></td>
						<td><?php echo $r['price']; ?></td>
						<td><?php echo $r['created_at']; ?></td>
						<td><?php echo $r['updated_at']; ?></td>
						<td>
							<a href="edit.php?edit=<?php echo $r['id']; ?>">Edit</a>
							<a href="admin-login.php?del=<?php echo $r['id']; ?>">Delete</a>
						</td>
					</tr>
				<?php } ?>
			</table>
			<?php 
			    include('config.php');
				if(isset($_GET['del'])){
					$id = $_GET['del'];
					mysqli_query($conn, "DELETE FROM books where id=$id");
				}
			?>



			<?php
			include('config.php');
			session_start();
			$issue = mysqli_query($conn, "SELECT students.name as student_name, books.name as book_name FROM books_issue LEFT JOIN students ON books_issue.student_id = students.id LEFT JOIN books ON books_issue.book_id = books.id"); ?>
		    <h2>Issued Books</h2>
			<table border="1px">
				<thead>
					<tr>
						<th>Student Name</th>
						<th>Book Name</th>
					</tr>
				</thead>
				
				<?php while ($r = mysqli_fetch_array($issue)) { ?>
					<tr>
						<td><?php echo $r['student_name']; ?></td>
						<td><?php echo $r['book_name']; ?></td>
					</tr>
				<?php } ?>
			</table>
</body>
</html>